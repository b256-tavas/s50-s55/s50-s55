import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2'

export default function LogIn () {
	
	// useContext allow us to consume the UserContext object and its properties for user validation
	// State hooks to store value of our input fields

	const {user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	

	useEffect (() => {
		if(email !== '' && password !== '' && password !== '') {

			setIsActive(true)

		} else {

			setIsActive(false)

		}
	})

	function loginUser (e) {

		e.preventDefault();
		// Set the email of the authenticated user in the local storage
		// Process a fetch request to the corresponding backend API
		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined
			    // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

			// to know if user is logged in or not
			if (typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} else {

				Swal.fire ({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})

		//localStorage.setItem("email", email)

		// below is use to refresh
		//setUser({
		//	email: localStorage.getItem("email")
		//})


		setEmail('');
		setPassword('');

		//alert('You are now logged in');
	}

	const retrieveUserDetails = (token) => {

		fetch ('http://localhost:4000/users/details', {
			header: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return (
		(user.id !== null)?
			<Navigate to='/courses' />
			:
		<Form onSubmit={e => loginUser(e)}>
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
	      </Form.Group>

	      {
	      	isActive ?
	      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	      	:
	      		<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
	      }
	      
	    </Form>
	)
}