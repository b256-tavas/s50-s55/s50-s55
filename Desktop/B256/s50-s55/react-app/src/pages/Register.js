import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

	const {user, setUser } = useContext(UserContext);
	// State hooks to store the values of our input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [number, setNumber] = useState('')
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// useEffect() - whenever there is a change in our webpage
	useEffect (() => {
		
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {

			setIsActive(true)

		} else {

			setIsActive(false)

		}
	})

	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data.email === true) {

				localStorage.setItem('email', data.email)
				retrieveUserDetails(data.email)

				Swal.fire({
					title: "Registering Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} else {

				Swal.fire ({
					title: "Registration failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})


		const retrieveUserDetails = (email) => {

		fetch ('http://localhost:4000/users/details', {
			header: {
				body: 'email'
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setUser({
				email: data.email,
			})
		})
	}
		// clear input fields
		setFirstName('')
		setLastName('')
		setNumber('')
		setEmail('');
		setPassword1('');
		setPassword2('');

	}

	// onChange - checks if there are any changes inside of the input fields.
	// value={email} - the value stroed in the input field will come from the value inside the getter "email"
	// setEmail(e.target.value) - sets the value of the getter ëmail to the value stored in the value={email} inside the input field
	return (

		(user.email === null)?
			<Navigate to='/courses' />
			:
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group className="mb-3" controlId="formFirstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control type="firstName" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formLastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control type="lastName" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formNumber">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control type="number" placeholder="Mobile Number" value={number} onChange={e => setNumber(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	      </Form.Group>

	  	  {/*Ternary Operator*/}
	  	  {/*
			if (isActive === true) {
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			} else {
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
	  	  */}
	      {
	      	isActive ?
	      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	      	:
	      		<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
	      }
	      
	    </Form>
	)
}
