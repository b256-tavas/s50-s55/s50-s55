import { useState, useEffect } from 'react';
//import CourseData from '../data/CourseData';
import Coursecard from '../components/Coursecard';

export default function Courses () {
	
	//console.log(CourseData)
	const [ courses, setCourses ] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/courses/active')
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setCourses(data.map(course => {
		return (
			<Coursecard key={course.id} courseProp={course} />
			)
	}))
		})
	})

	
	return (
		<>
			<h1>Courses</h1>
			{courses}

		</>
	// use if displaying a single course
	// <Coursecard courseProp={CourseData[0]} />
	)

}