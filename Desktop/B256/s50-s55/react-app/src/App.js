
import './App.css';
import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';

/*
The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint.
*/
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { UserProvider} from './userContext';
//import Banner from './components/Banner';

// add container
import {Container} from 'react-bootstrap';
//import Highlights from './components/Highlights'
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register'
import LogIn from './pages/LogIn'
import LogOut from './pages/LogOut'
import ErrorPage from './pages/ErrorPage'
import CourseView from './pages/CourseView'

function App() {

  // State hook for the user to store the information for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  
  // Fucntion for clearing the localStorage when a user is log out
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect (() => {
    console.log(user);
    console.log(localStorage);
  })

  return (
    // Fragments are needed if there are more 2 or more pages, components, or html file needed
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavBar />
      <Container>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses/>} />
          <Route path='/register' element={<Register/>} />
          <Route path='/login' element={<LogIn/>} />
          <Route path='/logout' element={<LogOut/>} />
          <Route path='*' element={<ErrorPage />} />
          <Route path="/courseView/:courseId" element={<CourseView />}/>
        </Routes>
      </Container>
    
    </Router>
    </UserProvider>
  );
}

export default App;
