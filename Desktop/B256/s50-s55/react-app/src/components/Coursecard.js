
import {useState} from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom'

// props or porperties act as a function parameter

/*
props = courseProp : { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}

*/

// deconstructing method

// const {name, description, price} = courseProp

// Add {} to pass/access exact value


export default function Coursecard ({courseProp}) {
//	console.log(props);
// console.log(typeof props);
 // const [count, setCount] = useState(30);

const { name, description, price, _id } = courseProp;
//getter -> stores the value using variable
//setter -> tells the computer how to store the value. sets the value to be stored in the getter
// (0) -> initial getter value
// setCount is a method to count + 1
/*
function enroll() {

    if (count > 0) {
      setCount(count - 1);
    } else {
      alert('No more seats');
    }
}
*/

  return (
	<Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
      <Card.Text>{price}</Card.Text>

        <Button variant="primary" as={Link} to={`/courseView/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
	)
}