// React Bootstrap Components
import { useContext } from 'react';
import { Container,Navbar,Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext'
//import Navbar from 'react-bootstrap/Navbar';
//import Nav from 'react-bootstrap/Nav';

export default function AppNavBar() {
	
// State hooks to store user info in the log in page

  //const [user, setUser]= useState(localStorage.getItem("email"));
  //console.log(user);

  const { user } = useContext(UserContext);

  return (
		<Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Zuitt Bootcamp</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
            {
              (user.id !== null)?
                <Nav.Link as={NavLink} to="/logout">LogOut</Nav.Link>
                :
                <>
                <Nav.Link as={NavLink} to="/login">LogIn</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>

            }
    
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

	)
}